//
//  favouriteCountries'ViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import CoreData
import ObjectMapper

class FavouriteCountriesViewController: BaseViewController {
    
    @IBOutlet weak var favouritesSearchBar: UISearchBar!
    @IBOutlet weak var favouritesTableView: UITableView!
    var favouriteCountriesArr : [AllCountryData]?
    var favouritesCountryDbData : [NSManagedObject] = []
    var allCountryDataObj:AllCountryData?
    var searchActive : Bool = false
    var filtered:[AllCountryData]?
    var favCountrySearchString : String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // fetch favourites list from core data
        self.favouritesSearchBar.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fnToFetcheData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

//MARK: Tableview delegate,datasource
extension FavouriteCountriesViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive){
            return self.filtered?.count ?? 0
        }else{
            return self.favouriteCountriesArr?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let countryHeight = favouriteCountriesArr![indexPath.row].countryName?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 18.0, weight: .bold))
        
        let capitalHeight = favouriteCountriesArr![indexPath.row].capital?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 18.0, weight: .bold))
        
        let subRegionHeight = favouriteCountriesArr![indexPath.row].subregion?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 18.0, weight: .bold))
        
        let totalHeight = countryHeight! + capitalHeight! + subRegionHeight! + 60
        return totalHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = Constants.ClassName.favouriteCountryCell
        favouritesTableView.register(UINib(nibName: Constants.ClassName.favouriteCountryCell, bundle: nil), forCellReuseIdentifier:cellIdentifier)
        let cell = favouritesTableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! FavouritesTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.delegate = self
        if(searchActive){
            cell.favouritesCountryData = filtered?[indexPath.row]
            
        }else{
            cell.favouritesCountryData = favouriteCountriesArr?[indexPath.row]
            
        }
        cell.allCountryData = favouriteCountriesArr?[indexPath.row]
        cell.favouriteContentView.layer.cornerRadius = 10.0
        cell.favouriteWebView.layer.cornerRadius =  cell.favouriteWebView.frame.size.width/2
        cell.deleteButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Utility.sharedInstance.defaults.set(Constants.Strings.isFromFavouritesValue, forKey: Constants.Strings.isFromFavourites)
        let countyDetailsVC = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.detailCountryViewController) as! CountryDetailsViewController
        countyDetailsVC.allCountryData = favouriteCountriesArr?[indexPath.row]
        countyDetailsVC.isFavourite = true
        self.navigationController?.pushViewController(countyDetailsVC, animated: true)
    }
}

//MARK: Favourites delegate
extension FavouriteCountriesViewController: FavouriteCountiresDeleagte
{
    // This method call is used to remove object from database on delete button action
    func fnForRemoveCountryFromFavourites(dataIndex: Int) {
        fnToRemoveData(objectIndexToRemove: dataIndex)
    }
}

//MARK: Core Data

extension FavouriteCountriesViewController
{
    // Fetching data from database and reloading tableview
    func fnToFetcheData(){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: Constants.Strings.entityName)
        do {
            
            fetchRequest.returnsObjectsAsFaults = false
            favouritesCountryDbData = try managedContext.fetch(fetchRequest) // array of managed objects
            
            var favCountryDbArr:[AllCountryData] = []
            for obj in favouritesCountryDbData {
                favCountryDbArr.append(fnToSaveDataToModelFromDb(favCountry:obj))
            }
            self.favouriteCountriesArr = favCountryDbArr // self.favouriteCountrieArr:- array of allCountryData objects
            print(self.favouriteCountriesArr as Any)
            
            self.favouritesTableView.reloadData()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func fnToSaveDataToModelFromDb(favCountry:NSManagedObject) -> AllCountryData{
        
        // Save data to model from database
        
        let allCountryObj = AllCountryData()
        
        if let dbCountryName = (favCountry.value(forKey: Constants.coreDataAtrNames.countryName)){
            allCountryObj.countryName = (dbCountryName as! String)
        }
        if let dbCapital = (favCountry.value(forKey: Constants.coreDataAtrNames.capital)){
            allCountryObj.capital = (dbCapital as! String)
        }
        if let dbregion = (favCountry.value(forKey: Constants.coreDataAtrNames.region)){
            allCountryObj.region = (dbregion as! String)
        }
        if let dbSubregion = (favCountry.value(forKey: Constants.coreDataAtrNames.subregion)){
            allCountryObj.subregion = (dbSubregion as! String)
        }
        if let dbBorders = (favCountry.value(forKey: Constants.coreDataAtrNames.borders)){
            allCountryObj.borders = (dbBorders as! [String])
        }
        if let dbFlag = (favCountry.value(forKey: Constants.coreDataAtrNames.flag)){
            allCountryObj.flag = (dbFlag as! String)
        }
        
        if let currancy = (favCountry.value(forKey: Constants.coreDataAtrNames.currency)){
            print(currancy)
        }
        return allCountryObj
    }
    
    func fnToRemoveData(objectIndexToRemove:Int){
        
        // Remove objects from database
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        managedContext.delete((favouritesCountryDbData[objectIndexToRemove]))
        self.favouriteCountriesArr?.remove(at: objectIndexToRemove)
        do {
            try managedContext.save()
        } catch _ {
        }
        // remove the deleted item from the `UITableView`
        self.favouritesTableView.reloadData()
        
        self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.favCountryDeleted, forActions: [Constants.Alert.alert_button_title_ok])
    }
    
    
}

//MARK: - UISearchBar Delegate-

extension FavouriteCountriesViewController: UISearchBarDelegate, UISearchDisplayDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var favCountrySearchArr:[AllCountryData] = []
        
        for object in favouriteCountriesArr!{
            for favCountry in [object] as [AllCountryData]{
                if let countryName = favCountry.countryName{
                    favCountrySearchString = countryName
                    let searchNameRng: NSRange? = (favCountrySearchString as NSString?)?.range(of: searchText, options: .caseInsensitive)
                    if searchNameRng?.location != NSNotFound {
                        favCountrySearchArr.append(favCountry)
                    }
                }
            }
        }
        
        filtered = favCountrySearchArr
        if(filtered?.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.favouritesTableView.reloadData()
    }
    
}

