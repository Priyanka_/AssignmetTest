//
//  allCountriesViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import CoreData
import ObjectMapper

class AllCountriesViewController: BaseViewController {
    
    
    @IBOutlet weak var allCountriesTableView: UITableView!
    var allCountriesArr : [AllCountryData]?
    var favouritesCountryList: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        Utility.sharedInstance.showLoadingIndicator()
        let manager = AllCountryApiManager()
        manager.countryRespnseDelegate = self
        manager.getAllCountry()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - Button Actions
extension BaseViewController
{
    @IBAction func fnForLogoutButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
            let loginVC = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.loginViewController) as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
}

//MARK: Tableview delegate,datasource

extension AllCountriesViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allCountriesArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = Constants.ClassName.allCountriesCell
        let cell = allCountriesTableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! AllCountriesTableViewCell
        cell.delegate = self
        cell.allCountryData = self.allCountriesArr?[indexPath.row]
        cell.favouritesCountryData = self.allCountriesArr?[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.allCountryView.layer.cornerRadius = 10.0
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        Utility.sharedInstance.defaults.set(Constants.Strings.isFromAllValue, forKey: Constants.Strings.isFromAll)
        let countyDetailsVC = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.detailCountryViewController) as! CountryDetailsViewController
        countyDetailsVC.allCountryData = self.allCountriesArr?[indexPath.row]
        self.navigationController?.pushViewController(countyDetailsVC, animated: true)
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let countryHeight = allCountriesArr![indexPath.row].countryName?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 15.0, weight: .bold))
        
        let capitalHeight = allCountriesArr![indexPath.row].region?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 15.0, weight: .bold))
        
        let subRegionHeight = allCountriesArr![indexPath.row].subregion?.calculatedHeight(withConstrainedWidth: self.view.frame.width-100, for: UIFont.systemFont(ofSize: 15.0, weight: .bold))
        
        let totalHeight = countryHeight! + capitalHeight! + subRegionHeight! + 70
        
        return totalHeight
    }
}

//MARK:-  Service Response Delegate -

extension AllCountriesViewController: CountryRespnseDelegate{
    func Successfull(response:Any){
        Utility.sharedInstance.hideLoadingIndicator()
        let objAllCountryData:AllCountryModel = response as! AllCountryModel
        self.allCountriesArr = objAllCountryData.arrData
        self.allCountriesTableView.reloadData()
    }
    
    func Failure(response:Any){
        
        self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.titleNoConnection, forActions: [Constants.Alert.alert_button_title_ok])
    }
}

//MARK: Favourites Delegate

extension AllCountriesViewController: AllCountiresDeleagte
{
    /*****************************************************************************
     * Function :   Add country to favourite list
     * Comment  :   First check the country already existed in favourite list. if exstited show alert else add the country to favourite list
     *
     ****************************************************************************/
    func fnForAddCountryToFavourites(allCountryData: AllCountryData) {
        if allCountryData.countryName!.isCountryAlreadyExists() {
             self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.alreadyFavourite, forActions: ["ok"])
        }else{
            self.fnForSaveData(favCountryData: allCountryData)

        }
    }

}


//MARK: Core Data

extension AllCountriesViewController
{
    
    /*****************************************************************************
     * Function :   Save country data which marked as favourite into the database
     * Comment  :   access the entity and save the favourite country data to database
     ****************************************************************************/
    func fnForSaveData(favCountryData:AllCountryData)
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: Constants.Strings.entityName,
                                       in: managedContext)!
        
        let country = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        country.setValue(favCountryData.countryName, forKey: Constants.coreDataAtrNames.countryName)
        country.setValue(favCountryData.capital, forKey: Constants.coreDataAtrNames.capital)
        country.setValue(favCountryData.region, forKey: Constants.coreDataAtrNames.region)
        country.setValue(favCountryData.subregion, forKey: Constants.coreDataAtrNames.subregion)
        country.setValue(favCountryData.borders, forKey: Constants.coreDataAtrNames.borders)
        country.setValue(favCountryData.flag, forKey: Constants.coreDataAtrNames.flag)
        let jsonStrLanguage = Mapper().toJSONString(favCountryData.languages!, prettyPrint: true)
        let jsonStrCurrancies = Mapper().toJSONString(favCountryData.currencies!, prettyPrint: true)
        country.setValue(jsonStrLanguage, forKey: Constants.coreDataAtrNames.languages)
        country.setValue(jsonStrCurrancies, forKey: Constants.coreDataAtrNames.currency)
        
        do {
            try managedContext.save()
            favouritesCountryList.append(country)
            self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.favCountryAdded, forActions: [Constants.Alert.alert_button_title_ok])

        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
}

