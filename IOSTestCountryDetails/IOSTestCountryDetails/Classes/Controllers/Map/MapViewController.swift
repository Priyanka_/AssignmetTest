//
//  MapViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import GoogleMaps


class MapViewController: BaseViewController {

    @IBOutlet weak var countryMapView: GMSMapView!
    var allCountriesArr : [AllCountryData]?

    override func viewDidLoad() {
        super.viewDidLoad()

        fnToGetAllCountries()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: Mapview Methods
extension MapViewController
{
    func fnToGetAllCountries(){
        Utility.sharedInstance.showLoadingIndicator()
        let manager = AllCountryApiManager()
        manager.countryRespnseDelegate = self
        manager.getAllCountry()
    }
    
    func fnToLOadMapView(){
        
        // This method is for placing the pins on map by accesing countries latitude and longitude from model and loading the mapview
        
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)

        // Creates a marker in the center of the map.
        if let allCountires = self.allCountriesArr{
            for object in allCountires{
                let marker = GMSMarker()

                if(object.mapLatLong?.count == 0){
                }else{
                     marker.position = CLLocationCoordinate2D(latitude: object.mapLatLong![0], longitude: object.mapLatLong![1])
                }
                marker.title = object.countryName
                //marker.snippet = "Australia"
                marker.icon = UIImage(named: "location")
                marker.map = self.countryMapView
                //self.countryMapView = mapView
            }
        }

    }
}

//MARK: Service Response Delegate

extension MapViewController: CountryRespnseDelegate
{
    func Successfull(response:Any){
        Utility.sharedInstance.hideLoadingIndicator()
        let objAllCountryData:AllCountryModel = response as! AllCountryModel
        self.allCountriesArr = objAllCountryData.arrData
        fnToLOadMapView()

    }
    
    func Failure(response:Any){
        
        self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.titleNoConnection, forActions: [Constants.Alert.alert_button_title_ok])
    }
}
