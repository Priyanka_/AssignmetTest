//
//  BaseViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
 var alertView : UIAlertController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

// MARK: - Button Actions
extension BaseViewController
{
    @IBAction func fnForBackButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        }
    }
}


//MARK: - Custom Methods -

extension BaseViewController
{
    func showAlertWith(withTitle titleString: String, andMessage messageString: String, forActions actionsArray: Array<String>)
    {
        alertView = nil
        alertView = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        
        for (_,actionString) in actionsArray.enumerated()
        {
            alertView.addAction(UIAlertAction(title: actionString, style: UIAlertActionStyle.default, handler: nil))
        }
        
        DispatchQueue.main.async
            {
                self.present(self.alertView, animated: true, completion: nil)
        }
}
}
