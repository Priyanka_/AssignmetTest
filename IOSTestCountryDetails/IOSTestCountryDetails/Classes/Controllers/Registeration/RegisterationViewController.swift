//
//  registerationViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit

class RegisterationViewController: BaseViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.passwordTextField.isSecureTextEntry = true
        self.confirmPasswordTextField.isSecureTextEntry = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: Button Actions

extension RegisterationViewController{
    
    @IBAction func fnForRegisterButtonPressed(_ sender: Any) {
        registerUser()
    }
}


//MARK: TextField delegate

extension RegisterationViewController{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField.tag == 2) // return for confirm password
        {
            self.view.endEditing(true)
        }
        return false
    }
        
}

//MARK: Custom Methods
extension RegisterationViewController{
    
    func validateUserInfo() -> Bool
    {
        // check for empty fields
        if (userNameTextField.text == Constants.Strings.emptyString || passwordTextField.text == Constants.Strings.emptyString)
        {
            self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.messageEmptyFields, forActions: [Constants.Alert.alert_button_title_ok])
            return false
        }
        
        //  check password and confirm password
        if(passwordTextField.text != confirmPasswordTextField.text){
            
            self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.passwordMatch, forActions: [Constants.Alert.alert_button_title_ok])
            return false
        }
        
        // check for valid email
        if (!(userNameTextField.text?.isValidEmailId())!)
        {
            self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.messageInvalidMail, forActions: [Constants.Alert.alert_button_title_ok])
            return false
        }
        
        // when everything is validated return true
        return true
    }
    
    /*****************************************************************************
     * Function :   Register New User
     * Input    :   username and password
     ****************************************************************************/
    func registerUser(){
        if(validateUserInfo()){
            
            Utility.sharedInstance.defaults.set(Constants.LoginStatus.loginStatusValue, forKey: Constants.LoginStatus.loginStatus)
            Utility.sharedInstance.defaults.set(self.userNameTextField.text, forKey: Constants.UserInfo.userName)
            Utility.sharedInstance.defaults.set(self.passwordTextField.text, forKey: Constants.UserInfo.password)
            let objTabBar = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.tabBar) as! UITabBarController
            objTabBar.selectedIndex = 0
            self.navigationController?.pushViewController(objTabBar, animated: true)
        }
    }
    
}
