//
//  CountryDetailsViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import WebKit

class CountryDetailsViewController: BaseViewController {

    //var countryDetails:AllCountryData?
    var countryNameArr = [String]()
    var currencyArr = [Currencies]()
    var languagesArr = [Languages]()
    var bordersArr = [String]()
    var allCountryData:AllCountryData?
    var flagImage : String = ""
    var countryName = ""
    var isFavourite:Bool = false
    // New Outlets With Scrollview
    
    @IBOutlet weak var countryFlagWebView: WKWebView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var capitalNameLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var subRegionLabel: UILabel!
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var currencyCodeLabel: UILabel!
    @IBOutlet weak var currencySymbolLabel: UILabel!
    @IBOutlet weak var languagesNameLabel: UILabel!
    @IBOutlet weak var languagesNativeNameLabel: UILabel!
    @IBOutlet weak var bordersLabel: UILabel!
    @IBOutlet weak var favCountryStarImageView: UIImageView!
    
    @IBOutlet weak var countryDetailScrollView: UIScrollView!
    
    @IBOutlet weak var countryDetailView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        fnToLoadCountryData()
        countryDetailScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
        self.countryDetailScrollView.frame.size.height = countryDetailScrollView.contentSize.height
        self.countryDetailView.frame.size.height = self.countryDetailScrollView.frame.size.height
        
        
       // self.countryDetailsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: Custom Methods

extension CountryDetailsViewController
{
    func fnToLoadCountryData(){
        
        //* Laoding the country details from allCountryData model
        
        if isFavourite{
            self.favCountryStarImageView.isHidden = false
        }else{
            self.favCountryStarImageView.isHidden = true
        }
        
        if let bordersArray = self.allCountryData?.borders{
            if bordersArray.count>0{
                self.bordersArr = bordersArray
                self.bordersLabel.text = self.bordersArr[0]
            }else{
                self.bordersLabel.text = ""
            }
            
        }
        if let curencyArray = self.allCountryData?.currencies{
            self.currencyArr = curencyArray
            self.currencyCodeLabel.text = self.currencyArr[0].code
            self.currencyNameLabel.text = self.currencyArr[0].name
            self.currencySymbolLabel.text = self.currencyArr[0].symbol
        }
        
        if let languagesArray = self.allCountryData?.languages{
            self.languagesArr = languagesArray
            self.languagesNameLabel.text = self.languagesArr[0].name
            self.languagesNativeNameLabel.text = self.languagesArr[0].nativeName
        }
        
        if let countryCapital = self.allCountryData?.capital{
            //self.countryNameArr.append(countryCapital)
            self.capitalNameLabel.text = countryCapital
        }
        
        if let countryName = self.allCountryData?.countryName{
            //self.countryName = countryName
            self.countryNameLabel.text = countryName
        }
        
        if let regionName = self.allCountryData?.region{
            //self.countryNameArr.append(regionName)
            self.regionLabel.text = regionName
        }
        
        if let subRegionName = self.allCountryData?.subregion{
            //self.countryNameArr.append(subRegionName)
            self.subRegionLabel.text = subRegionName
        }
        
        if let flagImageString = self.allCountryData?.flag{
            self.flagImage = flagImageString
        }
        
        if let imageURL = URL(string: self.flagImage)
        {
            //Creating a page request which will load our URL (Which points to our path)
            let request: NSURLRequest = NSURLRequest(url: imageURL as URL)
            self.countryFlagWebView.load(request as URLRequest)  //Telling our webView to load our above request
            self.countryFlagWebView.contentMode = .scaleAspectFill
      
        }

    }
}

