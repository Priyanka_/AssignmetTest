//
//  loginViewController.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextfield.tag = 1
        self.userNameTextfield.keyboardType = .emailAddress
        self.passwordTextfield.isSecureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
}

//MARK: - UITextField Delegate -

extension LoginViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.count)! >= Constants.Limitations.kPasswordCharacterMaxLimit && (textField.tag == 1) {
            let oldLength = textField.text?.count
            let replacementLength = string.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            return newLength <= Constants.Limitations.kPasswordCharacterMaxLimit
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: - Button Actions -

extension LoginViewController
{
    // Login user
        @IBAction func fnForLoginButtonPressed(_ sender: Any) {
        
        if self.userNameTextfield.text?.count == 0 {
            self.userNameTextfield .becomeFirstResponder()
        }
        else if self.passwordTextfield.text?.count == 0 {
            self.passwordTextfield .becomeFirstResponder()
        }
        else if (!(userNameTextfield.text?.isValidEmailId())!) {
            
            self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.emailValidationMessage, forActions: [Constants.Alert.alert_button_title_ok])
        }
        else {
    
            getUserDetailInfo(withEmail: self.userNameTextfield.text!, andpassword: self.passwordTextfield.text!)
        }
    }
    
    @IBAction func fnForRegisterButtonPressed(_ sender: Any) {
        
        //Register new user
        let registerVC = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.registerationViewController) as! RegisterationViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
}


extension LoginViewController
{
    /*****************************************************************************
     * Function :   Login already registerd user
     *Input     :   username and password
     * Comment  :  If user is already registerd navigate to home else show alert
     ****************************************************************************/
     func getUserDetailInfo(withEmail email: String, andpassword password: String)
     {
        
        if (Utility.sharedInstance.defaults.object(forKey: Constants.UserInfo.userName)) != nil{
            if((self.userNameTextfield.text==(Utility.sharedInstance.defaults.object(forKey: Constants.UserInfo.userName) as! String)) && (self.passwordTextfield.text==(Utility.sharedInstance.defaults.object(forKey: Constants.UserInfo.password) as! String))){
                print(Utility.sharedInstance.defaults.object(forKey: Constants.UserInfo.userName) as! String)
                print(Utility.sharedInstance.defaults.object(forKey: Constants.UserInfo.password) as! String)
                Utility.sharedInstance.defaults.set(Constants.LoginStatus.loginStatusValue, forKey: Constants.LoginStatus.loginStatus)
                
                let objTabBar = UIStoryboard(name: Constants.ClassName.main, bundle: nil).instantiateViewController(withIdentifier: Constants.ClassName.tabBar) as! UITabBarController
                objTabBar.selectedIndex = 0
                self.navigationController?.pushViewController(objTabBar, animated: true)
                
            }else{
                
                self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.invalidUser, forActions: [Constants.Alert.alert_button_title_ok])
            }
        }else{
            
              self.showAlertWith(withTitle: Constants.Alert.titleAlert, andMessage: Constants.Alert.invalidUser, forActions: [Constants.Alert.alert_button_title_ok])
        }
        
    }
}

