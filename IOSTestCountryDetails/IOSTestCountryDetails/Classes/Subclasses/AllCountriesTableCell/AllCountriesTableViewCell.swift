//
//  AllCountriesTableViewCell.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import Foundation

protocol AllCountiresDeleagte{
    func fnForAddCountryToFavourites(allCountryData:AllCountryData)
}

class AllCountriesTableViewCell: UITableViewCell {
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var resionLabel: UILabel!
    @IBOutlet weak var subresionLabel: UILabel!
    @IBOutlet weak var allCountryView: UIView!
    @IBOutlet weak var btnFav: UIButton!
    
    var delegate:AllCountiresDeleagte?
    var favouritesCountryData:AllCountryData?
    
    var allCountryData:AllCountryData? {
        didSet {
            if let countryName = self.allCountryData?.countryName{
                self.countryNameLabel.text = countryName
                if countryName.isCountryAlreadyExists(){
                    self.btnFav.setImage(UIImage(named:"filledStar"), for: .normal)
                }else{
                    self.btnFav.setImage(UIImage(named:"favourites"), for: .normal)
                }
            }
            if let resionName = self.allCountryData?.region{
                if (resionName != ""){
                    self.resionLabel.text = resionName
                }
            }else{
                self.resionLabel.text = Constants.Strings.constantLabelText
            }
            
            if let subresionName = self.allCountryData?.subregion{
                if (subresionName != ""){
                    self.subresionLabel.text = subresionName
                }
            }else{
                self.subresionLabel.text = Constants.Strings.constantLabelText
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func fnForAddToFavouritesButtonPressed(_ sender: Any) {
        
        self.delegate?.fnForAddCountryToFavourites(allCountryData: favouritesCountryData!)
    }
}
