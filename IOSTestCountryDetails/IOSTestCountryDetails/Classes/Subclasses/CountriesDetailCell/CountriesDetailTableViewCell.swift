//
//  CountriesDetailTableViewCell.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit

class CountriesDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var countryDetailsNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
