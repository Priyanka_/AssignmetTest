//
//  FavouritesTableViewCell.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import WebKit
protocol FavouriteCountiresDeleagte{
    func fnForRemoveCountryFromFavourites(dataIndex:Int)
}
class FavouritesTableViewCell: UITableViewCell {

    @IBOutlet weak var favouriteContentView: UIView!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var capitalLabel: UILabel!
    @IBOutlet weak var subregionLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var favouriteWebView: WKWebView!
    var delegate:FavouriteCountiresDeleagte?
    var favouritesCountryData:AllCountryData?
    
    var allCountryData:AllCountryData? {
        didSet {
            
            if let countryName = self.favouritesCountryData?.countryName{
                if (countryName != ""){
                    self.countryNameLabel.text = countryName
                }
            }
            if let capitalName = self.favouritesCountryData?.capital{
                if (capitalName != ""){
                self.capitalLabel.text = capitalName
                }
            }
            
            if let subresionName = self.favouritesCountryData?.subregion{
                if (subresionName != ""){
                    self.subregionLabel.text = subresionName
                }
            }
            if let flagImageString = self.favouritesCountryData?.flag{
                
                if let imageURL = URL(string: flagImageString)
                {
                    //Creating a page request which will load our URL (Which points to our path)
                    let request: NSURLRequest = NSURLRequest(url: imageURL as URL)
                    self.favouriteWebView.load(request as URLRequest)  //Telling our webView to load our above request
                    self.favouriteWebView.contentMode = .scaleAspectFill
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func fnForRemoveCountryFromFavouritesButtonPressed(_ sender: Any) {
        if let favouriteCountryData = favouritesCountryData{
            self.delegate?.fnForRemoveCountryFromFavourites(dataIndex: (sender as AnyObject).tag)
        }
    }
    
}
