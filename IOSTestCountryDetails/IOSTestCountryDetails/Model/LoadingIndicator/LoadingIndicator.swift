

import UIKit

/// LoadingIndicator
class LoadingIndicator: UIView
{
    @IBOutlet weak var largeActivityIndicator: UIActivityIndicatorView!
    
    
    /// Things to do when loading nib
    override func awakeFromNib()
    {
        largeActivityIndicator.isHidden = false
    }
    
    /// Load view on window
    func present()
    {
        DispatchQueue.main.async {
            self.frame = (UIApplication.shared.delegate?.window??.frame)!
            UIApplication.shared.delegate?.window??.addSubview(self)
            self.largeActivityIndicator.startAnimating()
        }
    }

    func dismiss()
    {
        largeActivityIndicator.stopAnimating()
        self.removeFromSuperview()
    }
}
