//
//  Constants.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import Foundation
import UIKit

class Constants: NSObject
{
    //MARK: - Screen Constants
    struct Screen
    {
        static let X : CGFloat = UIScreen.main.bounds.origin.x
        static let Y : CGFloat = UIScreen.main.bounds.origin.y
        static let Width : CGFloat = UIScreen.main.bounds.size.width
        static let Height : CGFloat = UIScreen.main.bounds.size.height
    }
    
    struct appDelegate {
        
        @available(iOS 10.0, *)
        static let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    //MARK: - Url Constants : Base Url
    struct URL
    {
        static let baseUrl = "https://restcountries.eu/rest/v2/"
    }
    
    //MARK:- Service url name
    struct ServiceURLS
    {
        static let allCountriesService = "all"

    }
    
    //MARK: - API Keys
    struct ApiKeys
    {
        static let googleMapKey = "AIzaSyCcDEYu-xtUZRRVAbrg6Sd0Xkdckt_wID8"
        
    }
    
     //MARK: - Color codes
    
    struct Colors
    {
        static let Navigation = UIColor(red: 235.0/255.0, green: 25.0/255.0, blue: 90.0/255.0, alpha: 1.0)
        static let tabBar = UIColor(red: 65.0/255.0, green: 65.0/255.0, blue: 65.0/255.0, alpha: 1.0)
        static let lightBackground = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        static let darkBackground = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        static let iconsGrey = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0)
    }
    
    //MARK : Static Strings
    struct Strings {
        static let emptyString              = ""
        static let countryNameString        = "Country Name"
        static let currencyString           = "Currency"
        static let languagesString          = "Languages"
        static let bordersString            = "Borders"
        static let othersString             = "Other"
        static let entityName               = "FavouriteCountry"
        static let isFromFavouritesValue    = "1"
        static let isFromAllValue           = "0"
        static let isFromFavourites         = "fromFavourites"
        static let isFromAll                = "fromAll"
        static let constantLabelText        = "NA"

    }
    
    //MARK:- Core Data Attributes
    
    struct coreDataAtrNames {
        static let countryName          = "countryName"
        static let capital              = "capital"
        static let region               = "region"
        static let subregion            = "subregion"
        static let borders              = "borders"
        static let languages            = "languages"
        static let currency             = "currency"
        static let currencyName         = "currencyName"
        static let currencyCode         = "currencyCode"
        static let currencySymbol       = "currencySymbol"
        static let languageName         = "languageName"
        static let languageNativeName   = "languageNativeName"
        static let flag                 = "flag"

    }
    
    //MARK: - Error Constants
    struct Errors
    {
        static let error = ""
        static let no_data_found = "No data found"
        static let server_failure = "Server failed try again"
        static let Exception = "Exception"
        
    }
    
    //MARK: - Login status

    struct LoginStatus {
        static let loginStatus = "loginStatus"
        static let loginStatusValue = "1"
        static let logoutStatusValue = "0"

    }
    
    //MARK: - User info

    struct UserInfo {
        static let userName = "userName"
        static let password = "password"
    }
    
    //MARK: - Class name string constants

    struct ClassName {
        
        static let main = "Main"
        static let loginViewController = "LoginViewController"
        static let registerationViewController = "RegisterationViewController"
        static let allCountryViewController = "AllCountriesViewController"
        static let favouritesCountryViewController = "FavouriteCountriesViewController"
        static let detailCountryViewController = "CountryDetailsViewController"
        static let countriesDetailCell = "CountriesDetailTableViewCell"
        static let allCountriesCell = "AllCountriesTableViewCell"
        static let tabBar = "UITabBarController"
        static let favouriteCountryCell = "FavouritesTableViewCell"
    }
   
    //MARK: -  Alert Constants
    
    struct Alert
    {
        static let titleAlert = "Alert"
        static let titleError = "Error"
        static let titleNoConnection = "No internet connection"
        static let messageEmptyFields = "Field is required"
        static let messageInvalidMail = "Email is invalid"
        static let messageNoConnection = "Please, check your internet connection"
        static let alert_button_title_ok = "OK"
        static let passwordMatch = "Your Passwords were not matching."
        static let invalidUser = "User is not valid"
        static let emailValidationMessage = "Please enter a valid Email Id"
        static let favCountryDeleted = "Country successfully removed from favourite list"
        static let favCountryAdded = "Country successfully added to favourite list"
        static let alreadyFavourite = "Country already exist in favourite list"

    }
    
    struct Limitations {
        static let kPasswordCharacterMinLimit = 8
        static let kPasswordCharacterMaxLimit = 16
    }
    
}

