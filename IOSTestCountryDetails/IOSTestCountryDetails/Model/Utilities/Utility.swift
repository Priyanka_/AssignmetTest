//
//  Utility
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import SystemConfiguration
import Security

class Utility: NSObject
{
 static let sharedInstance = Utility() // singleton instance
    
    
    var loadingIndicator : LoadingIndicator? // loading indicator instance
    let defaults = UserDefaults.standard // user defaults instance
    
    override init() {}
    
    
    let kSecClassValue = NSString(format: kSecClass)
    let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
    let kSecValueDataValue = NSString(format: kSecValueData)
    let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
    let kSecAttrServiceValue = NSString(format: kSecAttrService)
    let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
    let kSecReturnDataValue = NSString(format: kSecReturnData)
    let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

    // Method to identify if Device is a Ipad or not.
 func isIpad() -> Bool
    {
        return UIDevice.current.userInterfaceIdiom == .pad ? true : false
    }
    
    
    // Save value to user defaults
 func saveValueToUserDefault(_ value: AnyObject, key : String) -> Void
    {
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    // Get value from defaults
 func getValueFromUserDefault( _ key : String) -> AnyObject?
    {
        return UserDefaults.standard.value(forKey: key)! as AnyObject
    }
    
    // Remove object from defaults
 func removeValuesFromUserDefault( _ key : String)
    {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    

    // To print device log
 func printLog(_ logMessage: String, functionName: String = #function)
    {
        print("\(functionName):\(logMessage)")
    }
    
    // This function is used to check internet connection
 func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                
                SCNetworkReachabilityCreateWithAddress(nil, $0)
                
            }
            
        }) else {
            
            return false
        }
        var flags = SCNetworkReachabilityFlags()
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags)
        {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }


    //Add loader indicator
    
    /// Display loading indicator
 func showLoadingIndicator()
    {
        if (loadingIndicator == nil)
        {
            loadingIndicator = Bundle.main.loadNibNamed("LoadingIndicator", owner: self, options: nil)?[0] as? LoadingIndicator
        }
        
        DispatchQueue.main.async
            {
                self.loadingIndicator?.present()
        }
    }
    
    /// Hide loading indicator
func hideLoadingIndicator()
    {
        DispatchQueue.main.async
            {
                self.loadingIndicator?.dismiss()
                //self.loadingIndicator = nil
        }
    }
    
  
}





