//
//  AllCountriesService.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import ObjectMapper


class AllCountryModel:Mappable{
    var arrData: [AllCountryData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        arrData  <- map ["data"]
    }
}

class AllCountryData:NSObject,Mappable{
    var countryName     :String?
    var alphaThreeCode  :String?
    var alphaTwoCode    :String?
    var area            :Int?
    var capital         :String?
    var cioc            :String?
    var demonym         :String?
    var flag            :String?
    var gini            :Double?
    var nativeName      :String?
    var numericCode     :String?
    var population      :Int32?
    var region          :String?
    var subregion       :String?
    var currencies      :[Currencies]?
    var borders         :[String]?
    var languages       :[Languages]?
    var mapLatLong      :[Double]?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        countryName     <- map["name"]
        alphaThreeCode  <- map["alpha3Code"]
        alphaTwoCode    <- map["alpha2Code"]
        area            <- map["area"]
        capital         <- map["capital"]
        cioc            <- map["cioc"]
        demonym         <- map["demonym"]
        flag            <- map["flag"]
        gini            <- map["gini"]
        nativeName      <- map["nativeName"]
        numericCode     <- map["numericCode"]
        population      <- map["population"]
        region          <- map["region"]
        subregion       <- map["subregion"]
        currencies      <- map["currencies"]
        borders          <- map["borders"]
        languages       <- map["languages"]
        mapLatLong      <- map["latlng"]
    }
}

class AllCourancyModel:NSObject, Mappable{
    var arrData: [Currencies]?
    
    required init?(map: Map){
        
    }
    
    override init() {
        super.init()
    }
    
    func mapping(map: Map) {
        arrData  <- map ["data"]
    }
}

class Currencies:NSObject, Mappable{
    
    var code            :String?
    var name            :String?
    var symbol          :String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code     <- map["code"]
        name     <- map["name"]
        symbol   <- map["symbol"]
    }
}

class Languages: Mappable{
    
    var iso639_1            :String?
    var iso639_2            :String?
    var name                :String?
    var nativeName          :String?
    

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        iso639_1     <- map["iso639_1"]
        iso639_2     <- map["iso639_2"]
        name         <- map["name"]
        nativeName   <- map["nativeName"]
    }
}

import Foundation
import ObjectMapper


@objc protocol CountryRespnseDelegate {
    @objc optional func Successfull(response:Any)
    @objc optional func Failure(response:Any)
}

class AllCountryApiManager:NSObject {
    
    var countryRespnseDelegate:CountryRespnseDelegate?
    
    public func getAllCountry(){
        CloudManager().makeCloudRequest(url: "", parameter: [:], headers:[:], requestType: .get) { response in

            switch response?.result {
            case .success?:
                if let json = response?.result.value {
                    let dic:[String:Any] = ["data":json]
                    let data = Mapper<AllCountryModel>().map(JSON:dic)!
                    self.countryRespnseDelegate?.Successfull!(response: data)
                }else{
                    print("Response",response?.result.value)
                }
                break
            case .failure?:
                break
            case .none:
                break
            }            
        }
    }
    
}

