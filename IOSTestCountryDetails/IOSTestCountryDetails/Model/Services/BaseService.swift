//
//  BaseService.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import Alamofire

class CloudManager {
    
    let errorResponse:DataResponse<Any>? = nil
    func makeCloudRequest(url:String,parameter:[String:Any],headers:[String:String],requestType:HTTPMethod ,completionHandler: @escaping (DataResponse<Any>?) -> Void ){
        
        if Utility.sharedInstance.isConnectedToNetwork() {
            let Baseurl = Constants.URL.baseUrl+url
            if(requestType == .put || requestType == .post){
                
                Alamofire.request(Baseurl, method: requestType, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    completionHandler(response)
                }
            }else{
                Alamofire.request(Baseurl, method: requestType, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    completionHandler(response)
                }
            }
        }else{
            ///Put alert here
            completionHandler(errorResponse)
        }
    }
    
}

