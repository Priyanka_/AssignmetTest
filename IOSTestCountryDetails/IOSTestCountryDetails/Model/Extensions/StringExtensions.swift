//
//  StringExtensions.swift
//  IOSTestCountryDetails
//
//  Created by Priyanka Kagankar on 2/26/18.
//  Copyright © 2018 Priyanka. All rights reserved.
//

import UIKit
import CoreData

extension String
{
    // Validate email id
    //returns-> true or false
    func isValidEmailId( ) ->Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func calculatedHeight(withConstrainedWidth width: CGFloat, for font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func calculatedWidth(withConstrainedHeight height: CGFloat, for font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.width
    }
    
    /*****************************************************************************
     * Function :   Check country already existed in favourites or not
     * Comment  :   access the entity and check the country is already exist in entity or not using request predicates
     ****************************************************************************/
    
    public func isCountryAlreadyExists() -> Bool {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.Strings.entityName)
        fetchRequest.predicate = NSPredicate(format: "countryName = %@", self)
        fetchRequest.includesSubentities = false
        
        var entitiesCount = 0
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        
        do {
            entitiesCount = try managedContext.count(for: fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return entitiesCount > 0
    }
    
    
    
}
